ansible-digitalocean-node
=========

Use Ansible to provision a DigitalOcean droplet

Example Playbook
----------------

Just look at tests/test.yml

License
-------

MIT

Author Information
------------------

Trond Sjøvang, https://trond.sjovang.no
